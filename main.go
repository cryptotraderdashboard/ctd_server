package main

import (
	"bitstamp"
	"bittrex"
	"coinstruct"
	"coincap"
	"encoding/json"
	"exchangestatus"
	"flag"
	"gdax"
	"log"
	"net/http"
	_ "net/http/pprof"
	"poloniex"
	"scheduler"
	"strings"

	"github.com/golang/glog"
	"github.com/gorilla/mux"
)

var flagEnableProfile = flag.Bool("profile", false, "enable profiling with pprof at localhost:6060")

func init() {
	flag.Parse()
	if *flagEnableProfile {
		flag.Set("v", "10")
		flag.Set("logtostderr", "true")
		flag.Set("stderrthreshold", "INFO")
	}
	flag.Parse()
}

func main() {
	exchangestatus.Init()

	bitstamp.Init()
	bittrex.Init()
	gdax.Init()
	poloniex.Init()

	exchangestatus.StartExchangeMonitoring()
	scheduler.StartScheduler()

	router := mux.NewRouter()
	router.HandleFunc("/coin/{id}", getCoin).
		Methods("GET")

	router.StrictSlash(true)

	router.HandleFunc("/{exchange}/coins", getExchangeCoins).
		Methods("GET")

	router.HandleFunc("/{exchange}/coin/{id}", getExchangeCoin).
		Methods("GET")

	if *flagEnableProfile {
		go http.ListenAndServe(":6060", nil)
	}

	log.Fatal(http.ListenAndServe(":8000", router))
}

func getExchangeCoins(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var coins []coinstruct.Coin
	var err error
	switch params["exchange"] {
	case "gdax":
		coins = *gdax.GetCoins()
	case "poloniex":
		coins, err = poloniex.GetCoins()
	case "bittrex":
		coins = bittrex.GetCoins()
	case "bitstamp":
		coins, err = bitstamp.GetCoins()
	default:
		http.Error(w, "Invalid request", http.StatusBadRequest)
		return
	}

	if err != nil {
		glog.Errorln(err)
		http.Error(w, "Internal Server error", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(coins)
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}
}

func getExchangeCoin(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var jsonData *map[string]interface{}
	var err error
	switch params["exchange"] {
	case "gdax":
		jsonData, err = gdax.GetCoinStats(strings.ToUpper(params["id"]))
	case "poloniex":
		jsonData, err = poloniex.GetCoinStats(strings.ToUpper(params["id"]))
	case "bittrex":
		jsonData, err = bittrex.GetCoinStats(strings.ToUpper(params["id"]))
	case "bitstamp":
		jsonData, err = bitstamp.GetCoinStats(strings.ToUpper(params["id"]))
	}
	if err != nil {
		glog.Errorln(err)
		if strings.HasPrefix(err.Error(), "invalid coinId id:") {
			http.Error(w, error.Error(err), http.StatusBadRequest)
			return
		}
		http.Error(w, "server error", http.StatusInternalServerError)
		return

	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(jsonData)
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "server error", http.StatusInternalServerError)
		return
	}
}

func getCoin(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	coin := coinstruct.Coin{}
	coin = coincap.GetCoinCapCoin(params["id"])
	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(coin)
	if err != nil {
		log.Println(err)
	}
}
