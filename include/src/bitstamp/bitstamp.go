package bitstamp

import (
	"coinstruct"
	"decimalmath"
	"errors"
	"exchangestatus"
	"strings"
	"sync"
	"time"

	"github.com/golang/glog"
)

const apiURL = "https://www.bitstamp.net/api/"

var currencyTypes = map[string]string{
	"BTC": "crypto",
	"BCH": "crypto",
	"ETH": "crypto",
	"LTC": "crypto",
	"XRP": "crypto",
	"USD": "fiat",
	"EUR": "fiat",
}

var bitstampDataSet = struct {
	sync.RWMutex
	Coin map[string]interface{}
}{Coin: make(map[string]interface{})}

// Init builds cache data structurs
func Init() {
	buildBitstampDataset()
}

func isValidCoin(coinID string) bool {
	if _, ok := currencyTypes[coinID]; ok && currencyTypes[coinID] == "crypto" {
		return true
	}

	return false
}

func validProductStats(baseCurrency string, quoteCurrency string) bool {
	bitstampDataSet.RLock()
	defer bitstampDataSet.RUnlock()

	if _, ok := bitstampDataSet.Coin[baseCurrency].(map[string]interface{})[quoteCurrency]; ok {
		return true
	}

	return false
}

func isDataOld(coinID string, maxAgeSeconds int) bool {
	var dataOld = false

	bitstampDataSet.RLock()
	defer bitstampDataSet.RUnlock()
	coin := bitstampDataSet.Coin[coinID].(map[string]interface{})

	for quote := range currencyTypes {
		if !validProductStats(coinID, quote) {
			continue
		}
		dataAge := time.Since(
			time.Unix(coin[quote].(map[string]interface{})["QueryTimestamp"].(int64), 0)).Seconds()
		if int(dataAge) > maxAgeSeconds {
			dataOld = true
			return dataOld
		}
	}

	return dataOld
}

func buildJSON(coinID string) *map[string]interface{} {
	bitstampDataSet.RLock()
	defer bitstampDataSet.RUnlock()
	/*
		tell go that bitstampDataSet.Coin[coinId] is a map[string]interface{}
		and set bitstampData equal the coin we want
	*/
	bitstampData := bitstampDataSet.Coin[coinID].(map[string]interface{})
	/*
		jsonData will hold all of the data for the coin
		since id and displayname are static we can set them outside the loop
	*/
	jsonData := map[string]interface{}{
		"id":           coinID,
		"display_name": bitstampData["DisplayName"],
	}
	for quote := range currencyTypes {
		if !validProductStats(coinID, quote) {
			continue
		}
		/*
			quoteData hold the data for the current quote currency in the loop,
			while quoteTmp is hold the structure for the json formatted data
		*/
		quoteData := bitstampData[quote].(map[string]interface{})
		quoteTmp := map[string]interface{}{
			strings.ToLower(quote) + "_price":           quoteData["Price"],
			strings.ToLower(quote) + "_24_hour_change":  quoteData["Delta"],
			strings.ToLower(quote) + "_query_timestamp": quoteData["QueryTimestamp"],
		}

		for k, v := range quoteTmp {
			jsonData[k] = v
		}
	}
	/*jsonString, _ := json2.Marshal(jsonData)

	fmt.Println(string(jsonString))*/

	return &jsonData
}

func buildBitstampDataset() {
	uP, _, err := getOnlineProducts()
	if err != nil && uP == nil {
		glog.Errorln("Unable to initialize Bitstamp package, check error log")
		exchangestatus.UpdateStatus("bitstamp", 0)
		return
	}
	exchangestatus.UpdateStatus("bitstamp", 1)

	if err != nil {
		glog.Errorln("Unable to get Bitstamp currencies")
		exchangestatus.UpdateStatus("bitstamp", 0)
		return
	}

	for c, t := range currencyTypes {
		//we only want to
		if t == "crypto" {
			updateCoinData(c, &uP)
		}
	}

	//TEST SECTION//
	//_ = build_json_struct("BTC")
	/*bitstampDataSet.RLock()
	jsonData, _ := json2.MarshalIndent(bitstampDataSet.Coin, "", " ")
	bitstampDataSet.RUnlock()
	fmt.Println(string(jsonData))*/
}

func updateCoinData(coinID string, onlineProducts *[]bitstampProducts) {
	glog.V(2).Infoln("update_coin_data " + coinID)

	if isValidCoin(coinID) {

		bitstampDataSet.Lock()
		defer bitstampDataSet.Unlock()

		coin := bitstampDataSet.Coin

		currencyNames := make(map[string]string)

		for _, currency := range *onlineProducts {
			splitName := strings.Split(currency.Name, "/")
			BaseCurrency := strings.Trim(strings.Split(currency.Description, "/")[0], " ")
			currencyNames[splitName[0]] = BaseCurrency
		}

		for _, product := range *onlineProducts {
			//only want products with the specified coin: coinId=BTC productId=BTC-USD/BTC-EUR
			if strings.HasPrefix(product.Name, coinID) {

				stats, err := getProductStats(product.URLSymbol)
				if err != nil {
					glog.Warningln("Failed to retriece stats for product: " + product.URLSymbol)
					continue
				}
				delta := decimalmath.CalculatePercentChangeFloat(stats.Open, stats.Last)
				/*
					build the structure for the coin:
					{
					 "DisplayName": "Bitcoin",
					 "USD": {
					  "Delta": -5.63,
					  "Price": 16336.62,
					  "QueryTimestamp": 1513825686
					 }
					}
				*/
				QuoteCurrency := strings.Trim(strings.Split(product.Name, "/")[1], " ")
				coinData := map[string]interface{}{
					"DisplayName": currencyNames[coinID],
					QuoteCurrency: map[string]interface{}{
						"Price":          stats.Last,
						"Delta":          delta,
						"QueryTimestamp": stats.Timestamp,
					},
				}

				if coin[coinID] == nil {
					coin[coinID] = coinData
				} else {
					/*
						since coin is a map[string]interface{} and an interface
						can be anything we havc tell go what it is
						in this case it's another map[string]interface{}
						but as far as go knows it could be map[stuct]string or anything else
					*/
					tmp := coin[coinID].(map[string]interface{})
					for k, v := range coinData {
						tmp[k] = v
					}
				}
			}
		}

	} else {
		glog.Warningln("update_coin_data: invalid Coin ID: " + coinID)
		return
	}
}

// GetCoins returns all active coins
func GetCoins() ([]coinstruct.Coin, error) {
	var coin coinstruct.Coin
	var coins []coinstruct.Coin

	var err error

	currencyNames := make(map[string]string)
	uP, _, err := getOnlineProducts()
	for _, currency := range uP {
		splitName := strings.Split(currency.Name, "/")
		BaseCurrency := strings.Trim(strings.Split(currency.Description, "/")[0], " ")
		currencyNames[splitName[0]] = BaseCurrency
	}

	for c, t := range currencyTypes {
		if err != nil {
			glog.Error(err)
			glog.Error("invalid response for Bitstamp currency: " + c)
		} else if t == "crypto" {
			coin.ID = c
			coin.DisplayName = currencyNames[c]
			coin.IsActive = true
			coins = append(coins, coin)
		} else {
			continue
		}
	}

	return coins, err
}

// GetCoinStats returns all data for coinID
func GetCoinStats(coinID string) (*map[string]interface{}, error) {
	if !isValidCoin(coinID) {
		err := errors.New("invalid coinId id: " + coinID)
		return nil, err
	}

	if isDataOld(coinID, 10) {
		uP, _, err := getOnlineProducts()
		if err != nil || uP == nil {
			glog.Errorln("Bitstamp package offline, check error log")
			exchangestatus.UpdateStatus("bitstamp", 0)
			return nil, errors.New("Bitstamp API is down")
		}
		exchangestatus.UpdateStatus("bitstamp", 1)
		updateCoinData(coinID, &uP)
	}

	jsonData := buildJSON(coinID)

	return jsonData, nil
}
