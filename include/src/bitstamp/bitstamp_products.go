package bitstamp

import (
	json2 "encoding/json"
	"restfulquery"

	"github.com/golang/glog"
)

type bitstampProducts struct {
	BaseDecimals    int    `json:"base_decimals"`
	MinimumOrder    string `json:"minimum_order"`
	Name            string `json:"name"`
	CounterDecimals int    `json:"counter_decimals"`
	Trading         string `json:"trading"`
	URLSymbol       string `json:"url_symbol"`
	Description     string `json:"description"`
}

func getProducts() ([]bitstampProducts, error) {
	bodyBytes, err := restfulquery.Get(apiURL + "v2/trading-pairs-info/")
	if err != nil {
		glog.Errorln(err)
		return nil, err
	}

	var products []bitstampProducts
	json2.Unmarshal(bodyBytes, &products)

	return products, nil
}

func getOnlineProducts() ([]bitstampProducts, []bitstampProducts, error) {
	products, err := getProducts()
	if err != nil {
		glog.Errorln(err)
		return nil, nil, err
	}

	var onlineProducts []bitstampProducts
	var offlineProducts []bitstampProducts

	//This will need to be fixed if we find a way to check if a coin is "online"

	/*
		for _, product := range products {
			if product.Status == "online" {
				onlineProducts = append(onlineProducts, product)
				onlineProductIds = append(onlineProductIds, product.Id)
			} else {
				offlineProducts = append(offlineProducts, product)
				glog.Warningln("skipped Bitstamp product: " + product.Id + " status: " + product.Status)
			}
		}*/
	onlineProducts = products
	return onlineProducts, offlineProducts, nil
}
