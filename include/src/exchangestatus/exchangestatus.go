package exchangestatus

import (
	"sync"
	"time"

	"github.com/golang/glog"
)

var exchanges = []string{"gdax", "coincap", "bittrex", "poloniex"}

/*
Status holder map, values:
	0 : offline
	1 : online
	2 : hasn't checked in
*/
var statusLock = struct {
	sync.RWMutex
	exchangeStatusHolder map[string]*status
}{exchangeStatusHolder: make(map[string]*status)}

type status struct {
	ExchangeName string
	Status       int
	LastUpdated  int64
}

// Init initialized local structures and mutexes
func Init() {
	statusLock.Lock()
	defer statusLock.Unlock()
	for _, exchange := range exchanges {
		exchangeHolder := statusLock.exchangeStatusHolder
		exchangeHolder[exchange] = &status{Status: 2,
			LastUpdated: time.Now().Unix()}
	}
}

// UpdateStatus takes the exchange and new status and updates status
func UpdateStatus(exchange string, newStatus int) {
	statusLock.Lock()
	defer statusLock.Unlock()
	exchangeHolder := statusLock.exchangeStatusHolder
	exchangeHolder[exchange] = &status{Status: newStatus,
		LastUpdated: time.Now().Unix()}
}

func checkStatus() []status {
	statusLock.RLock()
	defer statusLock.RUnlock()

	var exchangeStatuses []status
	exchangeHolder := statusLock.exchangeStatusHolder
	for exchange, value := range exchangeHolder {
		exchangeStatus := status{ExchangeName: exchange,
			Status:      value.Status,
			LastUpdated: value.LastUpdated,
		}
		exchangeStatuses = append(exchangeStatuses, exchangeStatus)
	}
	return exchangeStatuses
}

func watchStatus() {
	for {
		time.Sleep(5 * time.Second)
		exchangeStatuses := checkStatus()
		glog.Infoln(exchangeStatuses)
	}
}

// StartExchangeMonitoring start goroutine for monitoring exchange status
func StartExchangeMonitoring() {
	go watchStatus()
}
