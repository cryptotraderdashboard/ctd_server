package bittrex

import (
	"exchangestatus"
	"time"

	"github.com/golang/glog"
	"github.com/toorop/go-bittrex"
)

const marketSumDataOldDuration = 10 * time.Second

type bittrexMarketSum struct {
	MarketSum      []bittrex.MarketSummary
	queryTimestamp int64
}

func initMarketSummaries() (bS *bittrexMarketSum) {
	bS = &bittrexMarketSum{}
	bS.updateMarketSummaries()

	return
}

func (bS *bittrexMarketSum) updateMarketSummaries() {
	var timestamp = time.Now().Unix()

	marketSums, err := b.GetMarketSummaries()
	if err != nil {
		exchangestatus.UpdateStatus("bittrex", 0)

		return
	}

	*bS = bittrexMarketSum{
		MarketSum:      marketSums,
		queryTimestamp: timestamp,
	}
}

func (bS *bittrexMarketSum) updateData(force bool) {
	if force {
		glog.Infoln("force update bittrex market summaries called")
		bS.updateMarketSummaries()

		return
	}
	dataAge := time.Since(time.Unix(bS.queryTimestamp, 0))
	if dataAge > marketSumDataOldDuration {
		glog.Infoln("bittrex market summary data old, updating")
		bS.updateMarketSummaries()
	}
}

func (bS *bittrexMarketSum) getMarketSummary(marketID string) *bittrex.MarketSummary {
	for _, market := range bS.MarketSum {
		if market.MarketName == marketID {
			return &market
		}
	}

	return nil
}

func (bS *bittrexMarketSum) getMarketSummaries() *[]bittrex.MarketSummary {
	return &bS.MarketSum
}
