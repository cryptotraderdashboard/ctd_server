package bittrex

import (
	"coinstruct"
	"decimalmath"
	"errors"
	"exchangestatus"
	"strings"
	"sync"
	"time"

	"github.com/toorop/go-bittrex"
)

const (
	apiKey    = ""
	apiSecret = ""
)

var bittrexDataSet = struct {
	sync.RWMutex
	Coin map[string]interface{}
}{Coin: make(map[string]interface{})}

var b = bittrex.New(apiKey, apiSecret)

var bC *bittrexCurrencies

var bS *bittrexMarketSum

// Init builds cached data structures
func Init() {
	bC = initCurrencies()
	bS = initMarketSummaries()
	buildDataSet()
}

func isValidCoin(coinID string) bool {
	for _, coin := range bC.getCoins() {
		if coinID == coin.ID {
			return true
		}
	}

	return false
}

func isValidBaseMarket(marketID string) bool {
	for _, market := range bS.MarketSum {
		if strings.HasPrefix(market.MarketName, marketID) {
			return true
		}
	}

	return false
}

func isValidMarket(baseCurrency string, marketCurrency string) bool {
	bittrexDataSet.RLock()
	defer bittrexDataSet.RUnlock()

	if _, ok := bittrexDataSet.Coin[baseCurrency].(map[string]interface{})[marketCurrency]; ok {
		return true
	}
	return false

}

func isDataOld(coinID string, maxAgeSeconds int) bool {
	var dataOld = false

	bittrexDataSet.RLock()
	defer bittrexDataSet.RUnlock()

	coin := bittrexDataSet.Coin[coinID].(map[string]interface{})

	for market, v := range coin {
		switch v.(type) {
		case map[string]interface{}:
			dataAge := time.Since(
				time.Unix(coin[market].(map[string]interface{})["QueryTimeStamp"].(int64), 0)).Seconds()
			if int(dataAge) > maxAgeSeconds {
				dataOld = true
				return dataOld
			}
		}
	}

	return dataOld
}

func buildJSON(coinID string) *map[string]interface{} {
	bittrexDataSet.RLock()
	defer bittrexDataSet.RUnlock()

	bittrexData := bittrexDataSet.Coin[coinID].(map[string]interface{})

	jsonData := map[string]interface{}{
		"id":           coinID,
		"display_name": bittrexData["DisplayName"],
	}

	/*
		structure of bittrexData is
		{
		DisplayName: "Bitcoin",
			"marketCoin(ETH)": map[string]interface{}{
				Price: 1,
				Delta: 1,
				QueryTimeStamp: 1231231,
			},
		}
		in range market could be DisplayName: "Bitcoin" type map[string]string or
			"marketCoin(ETH)": map[string]interface{}{
				Price: 1,
				Delta: 1,
				QueryTimeStamp: 1231231,
			} type map[string]interface{}
		we only want it if it's type map[string]interface{}
	*/
	for market, v := range bittrexData {
		switch v.(type) {
		case map[string]interface{}:

			marketData := bittrexData[market].(map[string]interface{})
			marketTmp := map[string]interface{}{
				strings.ToLower(market) + "_price":           marketData["Price"],
				strings.ToLower(market) + "_24_hour_change":  marketData["Delta"],
				strings.ToLower(market) + "_query_timestamp": marketData["QueryTimeStamp"],
			}

			for k, v := range marketTmp {
				jsonData[k] = v
			}
		}
	}

	return &jsonData
}

func buildDataSet() {
	bittrexDataSet.Lock()
	defer bittrexDataSet.Unlock()

	coin := bittrexDataSet.Coin

	exchangestatus.UpdateStatus("bittrex", 1)

	currencyNames := make(map[string]string)

	for _, currency := range bC.getCoins() {
		currencyNames[currency.ID] = currency.DisplayName
	}

	bittrexMarkets := bS.getMarketSummaries()

	for _, market := range *bittrexMarkets {
		/*splitProduct := strings.Split(productId, "-")
		baseCurrency := splitProduct[0]
		quoteCurrency := splitProduct[1]*/
		splitStr := strings.Split(market.MarketName, "-")
		baseCurrency := splitStr[0]
		marketCurrency := splitStr[1]

		delta := decimalmath.CalculatePercentChangeDecimal(market.PrevDay, market.Last)
		timeStamp, _ := time.Parse(time.RFC3339, market.TimeStamp+"Z")
		priceFloat := decimalmath.ConvertDecToFloat64(market.Last)

		coinData := map[string]interface{}{
			"DisplayName": currencyNames[baseCurrency],
			marketCurrency: map[string]interface{}{
				"Price":          priceFloat,
				"Delta":          delta,
				"QueryTimeStamp": timeStamp.Unix(),
			},
		}

		if coin[baseCurrency] == nil {
			coin[baseCurrency] = coinData
		} else {
			tmp := coin[baseCurrency].(map[string]interface{})
			for k, v := range coinData {
				tmp[k] = v
			}
		}
	}
}

// GetCoins returns cached online currency data
func GetCoins() []coinstruct.Coin {
	return bC.getCoins()
}

// GetCoinStats returns cached data for coinID
func GetCoinStats(coinID string) (*map[string]interface{}, error) {
	if !isValidBaseMarket(coinID) {
		err := errors.New("invalid coinId id: " + coinID)
		return nil, err
	}

	jsonData := buildJSON(coinID)

	return jsonData, nil
}

// UpdateData calls all update methods to update cached data
func UpdateData(force bool) {
	bC.updateData(force)
	bS.updateData(force)
}
