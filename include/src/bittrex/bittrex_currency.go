package bittrex

import (
	"coinstruct"
	"errors"
	"exchangestatus"
	"time"

	"github.com/golang/glog"
)

const currencyDataOldDuration = 10 * time.Minute

type bittrexCurrencies struct {
	Currencies     []coinstruct.Coin
	queryTimestamp int64
}

func initCurrencies() (bC *bittrexCurrencies) {
	bC = &bittrexCurrencies{}
	bC.updateCurrencies()

	return
}

func (bC *bittrexCurrencies) updateCurrencies() {
	var tCoin coinstruct.Coin
	var tCoins []coinstruct.Coin
	var timestamp = time.Now().Unix()

	currencies, err := b.GetCurrencies()
	if err != nil {
		exchangestatus.UpdateStatus("bittrex", 0)

		return
	}

	for _, currency := range currencies {
		tCoin = coinstruct.Coin{
			ID:            currency.Currency,
			DisplayName:   currency.CurrencyLong,
			IsActive:      currency.IsActive,
			StatusMessage: currency.Notice,
		}
		tCoins = append(tCoins, tCoin)
	}

	exchangestatus.UpdateStatus("bittrex", 1)
	*bC = bittrexCurrencies{
		Currencies:     tCoins,
		queryTimestamp: timestamp,
	}
}

func (bC *bittrexCurrencies) updateData(force bool) {
	if force {
		glog.Infoln("force update bittrex currencies called")
		bC.updateCurrencies()
		return
	}
	dataAge := time.Since(time.Unix(bC.queryTimestamp, 0))
	if dataAge > currencyDataOldDuration {
		glog.Infoln("bittrex currency data old, updating")
		bC.updateCurrencies()
	}
}

func (bC *bittrexCurrencies) getCoins() (coins []coinstruct.Coin) {
	for _, currency := range bC.Currencies {
		if currency.IsActive {
			coins = append(coins, currency)
		}
	}

	return
}

func (bC *bittrexCurrencies) getCurrencies() *[]coinstruct.Coin {
	return &bC.Currencies
}

func (bC *bittrexCurrencies) getCurrency(id string) (*coinstruct.Coin, error) {
	for _, currency := range bC.Currencies {
		if id == currency.ID {
			return &currency, nil
		}
	}
	err := errors.New("invalid coin id: " + id)

	return nil, err
}
