package gdax

import (
	"coinstruct"
	json2 "encoding/json"
	"errors"
	"exchangestatus"
	"restfulquery"
	"time"

	"github.com/golang/glog"
)

const currencyDataOldDuration = 10 * time.Minute

var currencyTypes = map[string]string{
	"JPY": "fiat",
	"CAD": "fiat",
	"USD": "fiat",
	"EUR": "fiat",
	"GBP": "fiat",
}

type gdaxCurrencyAPI struct {
	ID      string `json:"id,omitempty"`
	Name    string `json:"name,omitempty"`
	MinSize string `json:"min_size,omitempty"`
	Status  string `json:"status,omitempty"`
	Message string `json:"message,omitempty"`
}

func getCurrency(id string) (*gdaxCurrencyAPI, error) {
	bodyBytes, err := restfulquery.Get(apiURL + "currencies/" + id)
	if err != nil {
		glog.Errorln(err)
		return nil, err
	}

	currency := gdaxCurrencyAPI{}
	if err := json2.Unmarshal(bodyBytes, &currency); err != nil {
		return &currency, err
	}

	return &currency, nil
}

func getCurrenciesAPI() (*[]gdaxCurrencyAPI, error) {
	bodyBytes, err := restfulquery.Get(apiURL + "currencies")
	if err != nil {
		glog.Errorln(err)
		return nil, err
	}

	var currencies []gdaxCurrencyAPI
	if err := json2.Unmarshal(bodyBytes, &currencies); err != nil {
		return &currencies, err
	}

	return &currencies, nil
}

type gdaxCurrencies struct {
	Currencies     []coinstruct.Coin
	queryTimestamp int64
}

func initCurrencies() *gdaxCurrencies {
	var gC = gdaxCurrencies{}
	gC.updateCoins()

	return &gC
}

func (gC *gdaxCurrencies) updateCoins() {
	var tCoin coinstruct.Coin
	var tCoins []coinstruct.Coin
	var timestamp = time.Now().Unix()

	currencies, err := getCurrenciesAPI()
	if err != nil || len(*currencies) < 1 {
		tCoins = []coinstruct.Coin{}

		exchangestatus.UpdateStatus("gdax", 0)

		return
	}

	for _, currency := range *currencies {
		var isActive bool
		if currency.Status == "online" {
			isActive = true
		} else {
			isActive = false
		}
		var isFiat = false
		if _, ok := currencyTypes[currency.ID]; ok {
			isFiat = true
		}
		var statusMessage = ""
		if currency.Message != "" {
			statusMessage = currency.Message
		}

		tCoin = coinstruct.Coin{
			ID:            currency.ID,
			DisplayName:   currency.Name,
			IsActive:      isActive,
			IsFiat:        isFiat,
			StatusMessage: statusMessage,
		}

		tCoins = append(tCoins, tCoin)
	}

	exchangestatus.UpdateStatus("gdax", 1)

	*gC = gdaxCurrencies{
		Currencies:     tCoins,
		queryTimestamp: timestamp,
	}
}

func (gC *gdaxCurrencies) Test() *[]coinstruct.Coin {

	return &gC.Currencies
}

func (gC *gdaxCurrencies) updateData(force bool) {
	if force {
		glog.Infoln("force update gdax currencies called")
		gC.updateCoins()
		return
	}
	dataAge := time.Since(time.Unix(gC.queryTimestamp, 0))
	if dataAge > currencyDataOldDuration {
		glog.Infoln("gdax currency data old, updating")
		gC.updateCoins()
	}
}

func (gC *gdaxCurrencies) getCoins() *[]coinstruct.Coin {
	var tmpCoins []coinstruct.Coin
	for _, currency := range gC.Currencies {
		if currency.IsActive && !currency.IsFiat {
			tmpCoins = append(tmpCoins, currency)
		}
	}

	return &tmpCoins
}

func (gC *gdaxCurrencies) getCurrencies() *[]coinstruct.Coin {
	return &gC.Currencies
}

func (gC *gdaxCurrencies) getCurrency(id string) (*coinstruct.Coin, error) {
	for _, currency := range gC.Currencies {
		if id == currency.ID {
			return &currency, nil
		}
	}
	err := errors.New("invalid coin id: " + id)

	return nil, err
}
