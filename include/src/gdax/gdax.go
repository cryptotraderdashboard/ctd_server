// Package gdax implements collecting and caching data from the gdax api
package gdax

import (
	"coinstruct"
	"decimalmath"
	"errors"
	"strings"
	"sync"
	"time"

	"github.com/golang/glog"
)

//apiURL is the url of the gdax api
const apiURL = "https://api.gdax.com/"

var gdaxDataSet = struct {
	sync.RWMutex
	Coin map[string]interface{}
}{Coin: make(map[string]interface{})}

var gC *gdaxCurrencies

var gP *gdaxProducts

var gS *gdaxStats

// Init initializes data structures and cache
func Init() {
	gC = initCurrencies()
	gP = initProducts()
	gS = initStats()
	buildGdaxDataset()
}

func isValidCoin(coinID string) bool {
	for _, coin := range *gC.getCoins() {
		if coinID == coin.ID {
			return true
		}
	}

	return false
}

func validProductStats(baseCurrency string, quoteCurrency string) bool {
	gdaxDataSet.RLock()
	defer gdaxDataSet.RUnlock()

	if _, ok := gdaxDataSet.Coin[baseCurrency].(map[string]interface{})[quoteCurrency]; ok {
		return true
	}
	return false
}

func isDataOld(coinID string, maxAgeSeconds int) bool {
	var dataOld = false

	gdaxDataSet.RLock()
	defer gdaxDataSet.RUnlock()
	coin := gdaxDataSet.Coin[coinID].(map[string]interface{})

	for _, quote := range *gC.getCurrencies() {
		if !validProductStats(coinID, quote.ID) {
			continue
		}
		dataAge := time.Since(
			time.Unix(coin[quote.ID].(map[string]interface{})["QueryTimestamp"].(int64), 0)).Seconds()
		if int(dataAge) > maxAgeSeconds {
			dataOld = true
			return dataOld
		}
	}

	return dataOld
}

func buildJSONStruct(coinID string) *map[string]interface{} {
	gdaxDataSet.RLock()
	defer gdaxDataSet.RUnlock()
	/*
		tell go that gdaxDataSet.Coin[coinID] is a map[string]interface{}
		and set gdaxData equal the coin we want
	*/
	gdaxData := gdaxDataSet.Coin[coinID].(map[string]interface{})
	/*
		jsonData will hold all of the data for the coin
		since id and displayname are static we can set them outside the loop
	*/
	jsonData := map[string]interface{}{
		"id":           coinID,
		"display_name": gdaxData["DisplayName"],
	}
	for _, quote := range *gC.getCurrencies() {
		if !validProductStats(coinID, quote.ID) {
			continue
		}
		/*
			quoteData hold the data for the current quote currency in the loop,
			while quoteTmp is hold the structure for the json formatted data
		*/
		quoteData := gdaxData[quote.ID].(map[string]interface{})
		quoteTmp := map[string]interface{}{
			strings.ToLower(quote.ID) + "_price":           quoteData["Price"],
			strings.ToLower(quote.ID) + "_24_hour_change":  quoteData["Delta"],
			strings.ToLower(quote.ID) + "_query_timestamp": quoteData["QueryTimestamp"],
		}

		for k, v := range quoteTmp {
			jsonData[k] = v
		}
	}
	/*jsonString, _ := json2.Marshal(jsonData)

	fmt.Println(string(jsonString))*/

	return &jsonData
}

func buildGdaxDataset() {
	currencies := gC.getCoins()

	for _, currency := range *currencies {
		updateCoinData(currency.ID, currency.DisplayName)
	}
}

func updateCoinData(coinID string, coinName string) {
	glog.V(2).Infoln("update_coin_data " + coinID)

	if isValidCoin(coinID) {

		gdaxDataSet.Lock()
		defer gdaxDataSet.Unlock()

		coin := gdaxDataSet.Coin

		for _, product := range gP.Products {
			//only want products with the specified coin: coinId=BTC productId=BTC-USD/BTC-EUR
			if strings.HasPrefix(product.ID, coinID) {

				stats := gS.getProductStats(product.ID)
				if stats == nil {
					glog.Warningln("Failed to retriece stats for product: " + product.ID)
					continue
				}
				delta := decimalmath.CalculatePercentChangeFloat(stats.Open, stats.Last)
				/*
					build the structure for the coin:
					{
					 "DisplayName": "Bitcoin",
					 "USD": {
					  "Delta": -5.63,
					  "Price": 16336.62,
					  "QueryTimestamp": 1513825686
					 }
					}
				*/
				coinData := map[string]interface{}{
					"DisplayName": coinName,
					product.QuoteCurrency: map[string]interface{}{
						"Price":          stats.Last,
						"Delta":          delta,
						"QueryTimestamp": stats.QueryTimestamp,
					},
				}

				if coin[coinID] == nil {
					coin[coinID] = coinData
				} else {
					/*
						since coin is a map[string]interface{} and an interface
						can be anything we havc tell go what it is
						in this case it's another map[string]interface{}
						but as far as go knows it could be map[stuct]string or anything else
					*/
					tmp := coin[coinID].(map[string]interface{})
					for k, v := range coinData {
						tmp[k] = v
					}
				}
			}
		}

	} else {
		glog.Warningln("update_coin_data: invalid Coin ID: " + coinID)
		return
	}
}

// GetCoins gets currently active cryptocurrencies
func GetCoins() *[]coinstruct.Coin {
	return gC.getCoins()
}

// GetCoinStats get the stats for all coin pairs coinID[*]
func GetCoinStats(coinID string) (*map[string]interface{}, error) {
	if !isValidCoin(coinID) {
		err := errors.New("invalid coinId id: " + coinID)
		return nil, err
	}

	/*if is_data_old(coinId, 10) {
		currency, _ := gC.get_currency(coinId)
		update_coin_data(coinId, currency.DisplayName)
	}*/

	jsonData := buildJSONStruct(coinID)

	return jsonData, nil
}

// UpdateData calls all private update functions for updating cache data
func UpdateData(force bool) {
	gC.updateData(force)
	gP.updateData(force)
	gS.updateData(force)
}
