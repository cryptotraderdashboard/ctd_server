package gdax

import (
	json2 "encoding/json"
	"errors"
	"exchangestatus"
	"restfulquery"
	"strconv"
	"time"

	"github.com/golang/glog"
)

const statsDataOldDuration = 10 * time.Second

type gdaxStatsAPIResponse struct {
	Product        string `json:"id"`
	Open           string `json:"open"`
	High           string `json:"high"`
	Low            string `json:"low"`
	Volume         string `json:"volume"`
	Last           string `json:"last"`
	Volume30Day    string `json:"volume_30day"`
	QueryTimestamp int64  `json:"query_timestamp"`
}

type gdaxStatAPI struct {
	Product        string  `json:"id"`
	Open           float64 `json:"open"`
	High           float64 `json:"high"`
	Low            float64 `json:"low"`
	Volume         float64 `json:"volume"`
	Last           float64 `json:"last"`
	Volume30Day    float64 `json:"volume_30day"`
	QueryTimestamp int64   `json:"query_timestamp"`
}

//noinspection ALL
func getStats() ([]gdaxStatAPI, error) {
	onlineProducts, _, err := getOnlineProducts()
	if err != nil {
		glog.Errorln(err)
		return nil, err
	}
	var stats []gdaxStatAPI

	for _, product := range onlineProducts {
		bodyBytes, err := restfulquery.Get(apiURL + "products/" + product.ID + "/stats")
		if err != nil {
			glog.Errorln(err)
			return nil, err
		}
		var queryStat = gdaxStatsAPIResponse{
			Product:        product.ID,
			QueryTimestamp: time.Now().Unix(),
		}
		json2.Unmarshal(bodyBytes, &queryStat)

		var floatStat gdaxStatAPI
		convertStats(&queryStat, &floatStat)
		stats = append(stats, floatStat)
	}

	return stats, nil
}

func getProductStats(productID string) (gdaxStatAPI, error) {
	var productStatAPI = gdaxStatsAPIResponse{
		Product:        productID,
		QueryTimestamp: time.Now().Unix()}

	var productStats gdaxStatAPI

	bodyBytes, err := restfulquery.Get(apiURL + "products/" + productID + "/stats")
	if err != nil {
		glog.Errorln(err)
		return productStats, err
	}
	json2.Unmarshal(bodyBytes, &productStatAPI)

	/*
		some of GDAX's products return 0 because they are not setup yet when they are
		converted to strings they end up as empty strings so here we disreguard them
	*/
	if productStatAPI.Last == "" && productStatAPI.Open == "" && productStatAPI.Low == "" &&
		productStatAPI.Volume30Day == "" && productStatAPI.Volume == "" && productStatAPI.High == "" {
		glog.Warningln("GDAX stats for product: " + productID + " are not populated, skipping")
		err := errors.New("GDAX stats for product: " + productID + " are not populated, skipping")
		return productStats, err
	}

	convertStats(&productStatAPI, &productStats)

	return productStats, nil
}

func convertStats(stringStatsStruct *gdaxStatsAPIResponse, floatStatsStruct *gdaxStatAPI) {
	//floatStatsStruct.Product = stringStatsStruct.Product
	//floatStatsStruct.QueryTimestamp = stringStatsStruct.QueryTimestamp
	fOpen, errO := strconv.ParseFloat(stringStatsStruct.Open, 64)
	fLast, errL := strconv.ParseFloat(stringStatsStruct.Last, 64)
	fHigh, errH := strconv.ParseFloat(stringStatsStruct.High, 64)
	fVolume, errV := strconv.ParseFloat(stringStatsStruct.Volume, 64)
	f30Day, err3 := strconv.ParseFloat(stringStatsStruct.Volume30Day, 64)
	fLow, errLow := strconv.ParseFloat(stringStatsStruct.Low, 64)
	errs := []error{errO, errL, errH, errV, err3, errLow}
	for _, err := range errs {
		if err != nil {
			glog.Errorln("Unable to convert GDAX strings to floats: ", err)
		}
	}
	*floatStatsStruct = gdaxStatAPI{
		Product:        stringStatsStruct.Product,
		QueryTimestamp: stringStatsStruct.QueryTimestamp,
		Open:           fOpen,
		Last:           fLast,
		High:           fHigh,
		Volume:         fVolume,
		Volume30Day:    f30Day,
		Low:            fLow,
	}
	//floatStatsStruct.Open = fOpen
	//floatStatsStruct.Last = fLast
	//floatStatsStruct.High = fHigh
	//floatStatsStruct.Volume = fVolume
	//floatStatsStruct.Volume30Day = f30Day
	//floatStatsStruct.Low = fLow
}

type gdaxStats struct {
	Stats          []gdaxStatAPI
	queryTimestamp int64
}

func initStats() *gdaxStats {
	var gS = gdaxStats{}
	gS.updateStats(gP)

	return &gS
}

func (gS *gdaxStats) updateStats(gP *gdaxProducts) {
	var timestamp = time.Now().Unix()
	var tmpStat gdaxStatAPI
	var tmpStats []gdaxStatAPI
	var err error

	for _, product := range *gP.getOnlineProducts() {
		tmpStat, err = getProductStats(product.ID)
		if err != nil {
			continue
		} else {
			tmpStats = append(tmpStats, tmpStat)
		}
	}

	exchangestatus.UpdateStatus("gdax", 1)

	*gS = gdaxStats{
		Stats:          tmpStats,
		queryTimestamp: timestamp,
	}
}

func (gS *gdaxStats) updateData(force bool) {
	if force {
		glog.Infoln("force update gdax stats called")
		gS.updateStats(gP)
		buildGdaxDataset()
		return
	}
	if dataAge := time.Since(time.Unix(gS.queryTimestamp, 0)); dataAge > statsDataOldDuration {
		glog.Infoln("gdax stats data old, updating")
		gS.updateStats(gP)
		buildGdaxDataset()
	}

}

func (gS *gdaxStats) getProductStats(productID string) *gdaxStatAPI {
	for _, stat := range gS.Stats {
		if stat.Product == productID {
			return &stat
		}
	}

	return nil
}

func (gS *gdaxStats) Test() *gdaxStatAPI {
	//return &gS.Stats
	return gS.getProductStats("BTC-USD")
}
