package gdax

import (
	json2 "encoding/json"
	"errors"
	"exchangestatus"
	"restfulquery"
	"time"

	"github.com/golang/glog"
)

const productDataOldDuration = 1 * time.Minute

type gdaxProductsAPI struct {
	ID            string `json:"id,omitempty"`
	BaseCurrency  string `json:"base_currency"`
	QuoteCurrency string `json:"quote_currency"`
	Status        string `json:"status,omitempty"`
	StatusMessage string `json:"status_message,omitempty"`
}

//noinspection ALL
func getProducts() (*[]gdaxProductsAPI, error) {
	bodyBytes, err := restfulquery.Get(apiURL + "products")
	if err != nil {
		glog.Errorln(err)
		return nil, err
	}

	var product []gdaxProductsAPI
	json2.Unmarshal(bodyBytes, &product)

	return &product, nil
}

//noinspection ALL
func getOnlineProducts() ([]gdaxProductsAPI, []gdaxProductsAPI, error) {
	products, err := getProducts()
	if err != nil {
		glog.Errorln(err)
		return nil, nil, err
	}

	var onlineProducts []gdaxProductsAPI
	var offlineProducts []gdaxProductsAPI

	for _, product := range *products {
		if product.Status == "online" {
			onlineProducts = append(onlineProducts, product)
		} else {
			offlineProducts = append(offlineProducts, product)
			glog.Warningln("skipped GDAX product: " + product.ID + " status: " + product.Status)
		}
	}

	return onlineProducts, offlineProducts, nil
}

type gdaxProduct struct {
	ID            string `json:"id,omitempty"`
	BaseCurrency  string `json:"base_currency,omitempty"`
	QuoteCurrency string `json:"quote_currency,omitempty"`
	IsActive      bool   `json:"is_active,omitempty"`
	StatusMessage string `json:"status_message,omitempty"`
}

type gdaxProducts struct {
	Products       []gdaxProduct
	queryTimestamp int64
}

func initProducts() *gdaxProducts {
	var gP = gdaxProducts{}
	gP.updateProducts()

	return &gP
}

func (gP *gdaxProducts) updateProducts() {
	var timestamp = time.Now().Unix()
	var tmpProduct gdaxProduct
	var tmpProducts []gdaxProduct

	apiProducts, err := getProducts()
	if err != nil || len(*apiProducts) < 1 {
		tmpProducts = []gdaxProduct{}

		exchangestatus.UpdateStatus("gdax", 0)
		return
	}

	for _, apiProduct := range *apiProducts {

		var isActive bool
		if apiProduct.Status == "online" {
			isActive = true
		} else {
			isActive = false
		}

		var statusMessage = ""
		if apiProduct.StatusMessage != "" {
			statusMessage = apiProduct.StatusMessage
		}

		tmpProduct = gdaxProduct{
			ID:            apiProduct.ID,
			BaseCurrency:  apiProduct.BaseCurrency,
			QuoteCurrency: apiProduct.QuoteCurrency,
			IsActive:      isActive,
			StatusMessage: statusMessage,
		}

		tmpProducts = append(tmpProducts, tmpProduct)
	}

	exchangestatus.UpdateStatus("gdax", 1)

	*gP = gdaxProducts{
		Products:       tmpProducts,
		queryTimestamp: timestamp,
	}
}

func (gP *gdaxProducts) Test() *[]gdaxProduct {
	return &gP.Products
}

func (gP *gdaxProducts) updateData(force bool) {
	if force {
		glog.Infoln("force update gdax products called")
		gP.updateProducts()
		return
	}
	if dataAge := time.Since(time.Unix(gP.queryTimestamp, 0)); dataAge > productDataOldDuration {
		glog.Infoln("gdax product data old, updating")
		gP.updateProducts()
	}

}

func (gP *gdaxProducts) getOnlineProducts() *[]gdaxProduct {
	var tmpProduct gdaxProduct
	var tmpProducts []gdaxProduct
	for _, product := range gP.Products {
		if product.IsActive {
			tmpProduct = product
			tmpProducts = append(tmpProducts, tmpProduct)
		}
	}

	return &tmpProducts
}

func (gP *gdaxProducts) getProducts() *[]gdaxProduct {
	return &gP.Products
}

func (gP *gdaxProducts) getProduct(id string) (*gdaxProduct, error) {
	for _, product := range gP.Products {
		if id == product.ID {
			return &product, nil
		}
	}
	err := errors.New("invalid product id: " + id)

	return nil, err
}
