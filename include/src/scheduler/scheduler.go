package scheduler

import (
	"bittrex"
	"gdax"
	"time"
)

const loopInterval = 3 * time.Second

// StartScheduler starts any goroutines that need to be run on a loop in the background
func StartScheduler() {
	go dataUpdateLoop()
}

func dataUpdateLoop() {
	for {
		time.Sleep(loopInterval)
		go gdax.UpdateData(false)
		go bittrex.UpdateData(false)
	}
}
