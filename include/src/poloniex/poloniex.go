// TODO implement caching
package poloniex

import (
	"coinstruct"
	"decimalmath"
	"errors"
	"exchangestatus"
	"strings"
	"sync"
	"time"

	"github.com/golang/glog"
)

const apiURL = "https://poloniex.com/public"

var poloniexDataSet = struct {
	sync.RWMutex
	Coin map[string]interface{}
}{Coin: make(map[string]interface{})}

var currencies = make(map[string]string)

// Init initializes data structures and cache
func Init() {
	buildDataSet()
}

func isValidCoin(coinID string) bool {
	if _, ok := currencies[coinID]; ok {
		return true
	}

	return false
}

func isDataOld(coinID string, maxAgeSeconds int) bool {
	var dataOld = false

	poloniexDataSet.RLock()
	defer poloniexDataSet.RUnlock()

	coin := poloniexDataSet.Coin[coinID].(map[string]interface{})

	for market, v := range coin {
		switch v.(type) {
		case map[string]interface{}:
			dataAge := time.Since(
				time.Unix(coin[market].(map[string]interface{})["QueryTimeStamp"].(int64), 0)).Seconds()
			if int(dataAge) > maxAgeSeconds {
				dataOld = true
				return dataOld
			}
		}
	}

	return dataOld
}

func floatToBool(f float64, inverse bool) bool {
	switch inverse {
	case true:
		if f == 0 {
			return false
		}
		return true
	}
	if f != 0 {
		return false
	}

	return true
}

func buildJSON(coinID string) *map[string]interface{} {
	poloniexDataSet.RLock()
	defer poloniexDataSet.RUnlock()

	poloniexData := poloniexDataSet.Coin[coinID].(map[string]interface{})

	jsonData := map[string]interface{}{
		"id":           coinID,
		"display_name": poloniexData["DisplayName"],
	}

	/*
		structure of poloniexData is
		{
		DisplayName: "Bitcoin",
			"marketCoin(ETH)": map[string]interface{}{
				Price: 1,
				Delta: 1,
				QueryTimeStamp: 1231231,
			},
		}
		in bittrexData range could be DisplayName: "Bitcoin" type map[string]string or
			"marketCoin(ETH)": map[string]interface{}{
				Price: 1,
				Delta: 1,
				QueryTimeStamp: 1231231,
			} type map[string]interface{}
		we only want it if it's type map[string]interface{}
	*/
	for market, v := range poloniexData {
		switch v.(type) {
		case map[string]interface{}:

			marketData := poloniexData[market].(map[string]interface{})
			marketTmp := map[string]interface{}{
				strings.ToLower(market) + "_price":           marketData["Price"],
				strings.ToLower(market) + "_24_hour_change":  marketData["Delta"],
				strings.ToLower(market) + "_query_timestamp": marketData["QueryTimeStamp"],
			}

			for k, v := range marketTmp {
				jsonData[k] = v
			}
		}
	}

	return &jsonData
}

func buildDataSet() {
	poloniexDataSet.Lock()
	defer poloniexDataSet.Unlock()

	coin := poloniexDataSet.Coin

	queryTime := time.Now().Unix()
	tickerData, err := getTicker()
	if err != nil {
		glog.Errorln(err)
	}
	if len(tickerData) == 0 {
		glog.Errorln("Poloniex didn't return any data")
		exchangestatus.UpdateStatus("poloniex", 0)
		return
	}

	exchangestatus.UpdateStatus("poloniex", 1)

	currencyData, err := getCurrencies()
	if err != nil {
		glog.Errorln(err)
	}

	for currencyPair, pairData := range tickerData {
		//Split currencyPair ("BTC_LTC") into base and market values ("BTC" and "LTC")
		splitPair := strings.Split(currencyPair, "_")
		baseCurrency := splitPair[0]
		marketCurrency := splitPair[1]

		//keep track of base currencies for use later in valid market verification
		currencies[baseCurrency] = "base"

		//type assertion for getting the currency info for the base name
		baseDisplayname := currencyData[baseCurrency].(map[string]interface{})["name"]

		/*
			priceFloat takes the string pairData["last"] and converts it to a
			float64 with the required type assertion if there is an error, skip
			the entire currency pair and report the error
		*/
		priceFloat, err := decimalmath.ConvertStringToFloat64(
			pairData.(map[string]interface{})["last"].(string),
			8,
			false)
		if err != nil {
			glog.Warningf("%s%s%s\n%s\n",
				"Skipping currency pair: ", currencyPair, "due to float conversion error:", err)
			continue
		}

		//same as priceFloat but for delta
		delta, err := decimalmath.ConvertStringToFloat64(
			pairData.(map[string]interface{})["percentChange"].(string),
			2,
			true)
		if err != nil {
			glog.Warningf("%s%s%s\n%s\n",
				"Skipping currency pair: ", currencyPair, "due to float conversion error:", err)
			continue
		}

		/*
			Build the underlying data structure for coin data
			if coin["BTC"] the data looks like the following:
			"DisplayName": "Bitcoin",
			"ETH": {
				"Delta": 1.55,
				"Price": 0.05107932,
				"QueryTimeStamp": 1514582938
			},
			"VTC": {
				"Delta": -0.19,
				"Price": 0.0004863,
				"QueryTimeStamp": 1514582938
			},
		*/
		coinData := map[string]interface{}{
			"DisplayName": baseDisplayname,
			marketCurrency: map[string]interface{}{
				"Price":          priceFloat,
				"Delta":          delta,
				"QueryTimeStamp": queryTime,
			},
		}

		if coin[baseCurrency] == nil {
			coin[baseCurrency] = coinData
		} else {
			tmp := coin[baseCurrency].(map[string]interface{})
			for k, v := range coinData {
				tmp[k] = v
			}
		}
	}
}

// GetCoins retrieves currently active cryptocurrencies
func GetCoins() ([]coinstruct.Coin, error) {
	var coin coinstruct.Coin
	var coins []coinstruct.Coin

	poloniexCurrencies, err := getCurrencies()
	if err != nil {
		glog.Errorln(err)
		return nil, err
	}

	for currency, data := range poloniexCurrencies {
		data := data.(map[string]interface{})
		if floatToBool(data["delisted"].(float64), true) {
			continue
		}
		var frozen = false
		if floatToBool(data["frozen"].(float64), true) {
			frozen = true
		}
		var active = true
		if floatToBool(data["disabled"].(float64), true) {
			active = false
		}
		coin.ID = currency
		coin.DisplayName = data["name"].(string)
		coin.IsFrozen = frozen
		coin.IsActive = active
		coins = append(coins, coin)
	}

	return coins, nil
}

// GetCoinStats returns all data for the coin pair coinID[*]
func GetCoinStats(coinID string) (*map[string]interface{}, error) {
	coinID = strings.ToUpper(coinID)
	if !isValidCoin(coinID) {
		err := errors.New("invalid coinId id: " + coinID)
		return nil, err
	}
	if isDataOld(coinID, 10) {
		buildDataSet()
	}

	jsonData := buildJSON(coinID)

	return jsonData, nil
}
