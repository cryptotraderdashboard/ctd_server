package poloniex

import (
	"decimalmath"
	json2 "encoding/json"
	"exchangestatus"
	"strings"
	"time"

	"github.com/golang/glog"
)

const statsDataOldDuration = 10 * time.Second

func getTicker() (map[string]interface{}, error) {
	bodyBytes, err := apiCallWrapper(apiURL + "?command=returnTicker")
	if err != nil {
		glog.Errorln(err)
		return nil, err
	}

	/*
		tickerData structure
		{
			"BTC_VTC": {
				"id": 100,
				"last": "0.00050451",
				"lowestAsk": "0.00050700",
				"highestBid": "0.00050452",
				"percentChange": "0.06749751",
				"baseVolume": "201.18918617",
				"quoteVolume": "418982.01792770",
				"isFrozen": "0",
				"high24hr": "0.00051842",
				"low24hr": "0.00043091"
			},
			"USDT_BTC": {
				"id": 121,
				"last": "14655.00000000",
				"lowestAsk": "14655.00000000",
				"highestBid": "14640.11308236",
				"percentChange": "0.03936170",
				"baseVolume": "99380539.61794215",
				"quoteVolume": "6990.21453774",
				"isFrozen": "0",
				"high24hr": "15100.00000000",
				"low24hr": "13335.00000000"
			},
		}
	*/
	var tickerData map[string]interface{}
	err = json2.Unmarshal(*bodyBytes, &tickerData)
	if err != nil {
		glog.Errorln(err)
		return nil, err
	}

	return tickerData, nil
}

type ticker struct {
	Market        string  `json:"market_id"`
	Base          string  `json:"base_id"`
	Quote         string  `json:"quote_id"`
	Last          float64 `json:"last"`
	PercentChange float64 `json:"percent_change"`
}

type poloniexStats struct {
	Stats          []ticker
	queryTimestamp int64
}

func initStats() *poloniexStats {
	var pS = poloniexStats{}
	pS.updateStats()

	return &pS
}

func (pS *poloniexStats) updateStats() {
	var timestamp = time.Now().Unix()
	var tmpStats []ticker

	poloniexData, err := getTicker()
	if err != nil {
		exchangestatus.UpdateStatus("poloniex", 0)
		return
	}

	for marketPair, marketData := range poloniexData {
		pairSplit := strings.Split(marketPair, "_")
		baseCur := pairSplit[0]
		quoteCur := pairSplit[1]

		lastF, err := decimalmath.ConvertStringToFloat64(
			marketData.(map[string]interface{})["last"].(string),
			8,
			false)
		if err != nil {
			glog.Warningln("%s%s%s\n%s\n",
				"Skipping currency pair: ", marketPair, "due to float conversion error:", err)
			continue
		}

		deltaF, err := decimalmath.ConvertStringToFloat64(
			marketData.(map[string]interface{})["percentChange"].(string),
			2,
			true)
		if err != nil {
			glog.Warningln("%s%s%s\n%s\n",
				"Skipping currency pair: ", marketPair, "due to float conversion error:", err)
			continue
		}

		tmpStat := ticker{
			Market:        baseCur + "-" + quoteCur,
			Base:          baseCur,
			Quote:         quoteCur,
			Last:          lastF,
			PercentChange: deltaF,
		}

		tmpStats = append(tmpStats, tmpStat)
	}

	exchangestatus.UpdateStatus("poloniex", 1)

	*pS = poloniexStats{
		Stats:          tmpStats,
		queryTimestamp: timestamp,
	}
}

func (pS *poloniexStats) updateData(force bool) {
	if force {
		glog.Infoln("force update poloniex called")
		pS.updateStats()
		return
	}
	if dataAge := time.Since(time.Unix(pS.queryTimestamp, 0)); dataAge > statsDataOldDuration {
		glog.Infoln("poloniex data old, updating")
		pS.updateStats()
	}
}

func (pS *poloniexStats) getMarketStats(marketID string) *ticker {
	for _, stat := range pS.Stats {
		if stat.Market == marketID {
			return &stat
		}
	}

	return nil
}
