package poloniex

import (
	"coinstruct"
	json2 "encoding/json"
	"errors"
	"exchangestatus"
	"time"

	"github.com/golang/glog"
)

const currencyDataOldDuration = 10 * time.Minute

// getCurrencies get currency data from poloniex in the following format
// currencyData structure
// {
// 	"BTC": {
// 		"id": 28,
// 		"name": "Bitcoin",
// 		"txFee": "0.00050000",
// 		"minConf": 1,
// 		"depositAddress": null,
// 		"disabled": 0,
// 		"delisted": 0,
// 		"frozen": 0
// 	},
// 	"ETH": {
// 		"id": 267,
// 		"name": "Ethereum",
// 		"txFee": "0.00500000",
// 		"minConf": 35,
// 		"depositAddress": null,
// 		"disabled": 0,
// 		"delisted": 0,
// 		"frozen": 0
// 	},
// }
func getCurrencies() (map[string]interface{}, error) {
	bodyBytes, err := apiCallWrapper(apiURL + "?command=returnCurrencies")
	if err != nil {
		glog.Errorln(err)
		return nil, err
	}

	var currencyData map[string]interface{}
	err = json2.Unmarshal(*bodyBytes, &currencyData)
	if err != nil {
		glog.Errorln(err)
		return nil, err
	}

	return currencyData, nil
}

type poloniexCurrency struct {
	Currencies     []coinstruct.Coin
	queryTimestamp int64
}

func initCurrencies() *poloniexCurrency {
	var pC = poloniexCurrency{}
	pC.updateCoins()

	return &pC
}

func (pC *poloniexCurrency) updateCoins() {
	var tCoin coinstruct.Coin
	var tCoins []coinstruct.Coin
	var timestamp = time.Now().Unix()

	currencies, err := getCurrencies()
	if err != nil {
		exchangestatus.UpdateStatus("poloniex", 0)

		return
	}

	for currency, cData := range currencies {
		tCoin = coinstruct.Coin{
			ID:          currency,
			DisplayName: cData.(map[string]interface{})["name"].(string),
			IsActive:    floatToBool(cData.(map[string]interface{})["disabled"].(float64), false),
			IsDelisted:  floatToBool(cData.(map[string]interface{})["delisted"].(float64), true),
			IsFrozen:    floatToBool(cData.(map[string]interface{})["frozen"].(float64), true),
		}

		tCoins = append(tCoins, tCoin)
	}

	exchangestatus.UpdateStatus("poloniex", 1)

	*pC = poloniexCurrency{
		Currencies:     tCoins,
		queryTimestamp: timestamp,
	}
}

func (pC *poloniexCurrency) updateData(force bool) {
	if force {
		glog.Infoln("force update poloniex currencies called")
		pC.updateCoins()
		return
	}
	dataAge := time.Since(time.Unix(pC.queryTimestamp, 0))
	if dataAge > currencyDataOldDuration {
		glog.Infoln("poloniex currency data old, updating")
		pC.updateCoins()
	}
}

func (pC *poloniexCurrency) getCoins() *[]coinstruct.Coin {
	var tCoins []coinstruct.Coin
	for _, currency := range pC.Currencies {
		if currency.IsActive && !currency.IsDelisted {
			tCoins = append(tCoins, currency)
		}
	}

	return &tCoins
}

func (pC *poloniexCurrency) getCurrencies() *[]coinstruct.Coin {
	return &pC.Currencies
}

func (pC *poloniexCurrency) getCurrency(id string) (*coinstruct.Coin, error) {
	for _, currency := range pC.Currencies {
		if id == currency.ID {
			return &currency, nil
		}
	}
	err := errors.New("invalid coin id: " + id)

	return nil, err
}
