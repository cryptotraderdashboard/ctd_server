package poloniex

import (
	json2 "encoding/json"
	"flag"
	"fmt"
	"testing"
)

func init() {
	if testing.Verbose() {
		flag.Set("v", "2")
		flag.Set("logtostderr", "true")
		flag.Set("stderrthreshold", "INFO")
	}
}

/*func TestGetTicker(t *testing.T) {
	data, err := get_ticker()
	if err != nil {
		glog.Errorln(err)
	}
	tmp := data["ETH_ZEC"].(map[string]interface{})["last"]
	fmt.Println(tmp)

jsonData, err := json2.MarshalIndent(data, "", " ")
	if err != nil {
		glog.Errorln(err)
	}

	fmt.Println(string(jsonData))

}*/

func TestGetCurrencies(t *testing.T) {
	pC := initCurrencies()

	coins := pC.getCoins()

	data, _ := json2.MarshalIndent(coins, "", "   ")
	fmt.Println(string(data))
}

func TestTicker(t *testing.T) {
	pS := initStats()

	data, err := json2.MarshalIndent(pS.Stats, "", "    ")
	if err != nil {
		fmt.Errorf("%s", err)
		t.FailNow()
	}
	fmt.Println(string(data))
}

func TestMarketStats(t *testing.T) {
	pS := initStats()

	data, err := json2.MarshalIndent(pS.getMarketStats("BTC-XRP"), "", "    ")
	if err != nil {
		fmt.Errorf("%s", err)
		t.FailNow()
	}
	fmt.Println(string(data))
}
