package poloniex

import (
	"restfulquery"
	"time"
)

var apiCalls = 0
var apiCallTime int64

func apiCallWrapper(url string) (*[]byte, error) {
	var bodyBytes []byte
	var err error
	switch apiCalls {
	case 0:
		apiCallTime = time.Now().Unix()
		bodyBytes, err = restfulquery.Get(url)
		apiCalls++
	case 6:
		timePassed := time.Since(time.Unix(apiCallTime, 0)).Seconds()
		if timePassed <= 1 {
			time.Sleep(time.Second)
		}
		apiCallTime = time.Now().Unix()
		bodyBytes, err = restfulquery.Get(url)
		apiCalls = 1
	default:
		bodyBytes, err = restfulquery.Get(url)
		apiCalls++
	}

	return &bodyBytes, err
}
